<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
    	
    	for($i = 1; $i <= 10; $i++){

			$karyawan = [
				'nama' => $faker->name,
    			'alamat' => $faker->address
			];

			$karyawan = App\Karyawan::create($karyawan);

			$telepon = [
				'karyawan_id' => $karyawan->id,
    			'nomor_telepon' => $faker->phoneNumber
			];	

			App\Telepon::create($telepon);		
 
    	}
    }
}
