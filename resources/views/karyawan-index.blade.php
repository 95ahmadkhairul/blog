@extends('master')

@section('aktif')
<li class="nav-item">
	<a class="nav-link" href="/blog">Home</a>
</li>
<li class="nav-item">
	<a class="nav-link" href="/pegawai">Pegawai</a>
</li>
<li class="nav-item">
	<a class="nav-link active" href="/karyawan">Karyawan</a>
</li>
<li class="nav-item">
	<a class="nav-link" href="/blog/tentang">Tentang</a>
</li>
<li class="nav-item">
	<a class="nav-link" href="/blog/kontak">Kontak</a>
</li>
@endsection

@section('judul_halaman', 'Home -> Karyawan')

@section('konten')
	<p class="text-right"><a href="/karyawan/tambah">Tambah data</a></p>
	<table class="display table table-bordered" style="width:100%">
		<thead>
			<tr>
				<th>Nama</th>
				<th>Alamat</th>
				<th>No Telepon</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			@foreach($karyawan as $k)
			<tr>
				<td>{{ $k->nama }}</td>
				<td>{{ $k->alamat }}</td>
				<td>{{ $k->telepon->nomor_telepon }}</td>
				<td>
					<a href="/karyawan/edit/{{ $k->id }}" class="btn btn-info"><i class="fa fa-edit"></i></a> 
					<a href="/karyawan/delete/{{ $k->id }}" class="btn btn-danger"> <i class="fa fa-close"></i></a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
@endsection