@extends('master')

@section('aktif')
<li class="nav-item">
	<a class="nav-link active" href="/blog">Home</a>
</li>
<li class="nav-item">
	<a class="nav-link" href="/pegawai">Pegawai</a>
</li>
<li class="nav-item">
	<a class="nav-link" href="/karyawan">Karyawan</a>
</li>
<li class="nav-item">
	<a class="nav-link" href="/blog/tentang">Tentang</a>
</li>
<li class="nav-item">
	<a class="nav-link" href="/blog/kontak">Kontak</a>
</li>
@endsection

@section('judul_halaman', 'Home')

@section('konten')
 	<p>Selamat datang di Blog yang isinya gak ada blog sama sekali, Blog ini dibuat untuk tujuan belajar Framework Laravel</P>
@endsection