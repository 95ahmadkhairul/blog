@extends('master')

@section('aktif')
<li class="nav-item">
	<a class="nav-link" href="/blog">Home</a>
</li>
<li class="nav-item">
	<a class="nav-link active" href="/pegawai">Pegawai</a>
</li>
<li class="nav-item">
	<a class="nav-link" href="/karyawan">Karyawan</a>
</li>
<li class="nav-item">
	<a class="nav-link" href="/blog/tentang">Tentang</a>
</li>
<li class="nav-item">
	<a class="nav-link" href="/blog/kontak">Kontak</a>
</li>
@endsection

@section('judul_halaman', 'Home -> Pegawai -> Tambah')

@section('konten')
	<p class="text-right"><a href="/pegawai">Lihat data</a></p>
	<form action="/pegawai/simpan" method="post">
		{{ csrf_field() }}
		<div class="form-group">
			<label for="nama">Nama</label> 
			<input class="form-control" type="text" name="nama" required>
		</div>
		<div class="form-group">
			<label for="jabatan">Jabatan</label>
			<input class="form-control" type="text" name="jabatan" required>
		</div>
		<div class="form-group">
			<label for="alamat">Alamat</label> 
			<textarea class="form-control" name="alamat" required></textarea>
		</div>
		<div class="form-group">
			<label for="nohp">No HP</label>
			<input class="form-control" type="text" pattern="[0-9]*" name="nohp" required>
		</div>
		<input class="btn btn-primary" type="submit" value="Simpan Data">
	</form>
@endsection