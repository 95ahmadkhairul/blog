@extends('master')

@section('aktif')
<li class="nav-item">
	<a class="nav-link" href="/blog">Home</a>
</li>
<li class="nav-item">
	<a class="nav-link" href="/pegawai">Pegawai</a>
</li>
<li class="nav-item">
	<a class="nav-link active" href="/karyawan">Karyawan</a>
</li>
<li class="nav-item">
	<a class="nav-link" href="/blog/tentang">Tentang</a>
</li>
<li class="nav-item">
	<a class="nav-link" href="/blog/kontak">Kontak</a>
</li>
@endsection

@section('judul_halaman', 'Home -> Karyawan -> Tambah')

@section('konten')
	<p class="text-right"><a href="/karyawan">Lihat data</a></p>
	<form action="/karyawan/update" method="post">
		{{ csrf_field() }}

		<input type="hidden" value="{{ $karyawan->id }}" name="id" />
		<div class="form-group">
			<label for="nama">Nama</label> 
			<input class="form-control" type="text" value="{{ $karyawan->nama }}" name="nama" />
		</div>

		<div class="form-group">
			<label for="alamat">Alamat</label>
			<textarea class="form-control" name="alamat" >{{ $karyawan->alamat }}</textarea>
		</div>

		<div class="form-group">
			<label for="nomor_telepon">No Telepon</label>
			<input class="form-control" type="text" value="{{ $karyawan->telepon->nomor_telepon }}" name="nomor_telepon" />
		</div>
        
        @if (count($errors) > 0)
	    	<div class="alert alert-danger">
	       	  	@foreach ($errors->all() as $error)
	            	{{ $error }} <br />
	            @endforeach   	
	    	</div>
		@endif

		<input class="btn btn-primary" type="submit" value="Update Data">
	</form>
@endsection

