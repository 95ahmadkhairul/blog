@extends('master')

@section('aktif')
<li class="nav-item">
	<a class="nav-link" href="/blog">Home</a>
</li>
<li class="nav-item">
	<a class="nav-link active" href="/pegawai">Pegawai</a>
</li>
<li class="nav-item">
	<a class="nav-link" href="/karyawan">Karyawan</a>
</li>
<li class="nav-item">
	<a class="nav-link" href="/blog/tentang">Tentang</a>
</li>
<li class="nav-item">
	<a class="nav-link" href="/blog/kontak">Kontak</a>
</li>
@endsection

@section('judul_halaman', 'Home -> Pegawai -> Edit Data')

@section('konten')
	<p class="text-right"><a href="/pegawai">Lihat data</a></p>
	@foreach($pegawai as $p)
	<form action="/pegawai/update" method="post">
		{{ csrf_field() }}
		<input type="hidden" value="{{ $p->pegawai_id }}" name="id" />
		<div class="form-group">
			<label for="nama">Nama</label> 
			<input class="form-control" type="text" value="{{ $p->pegawai_nama }}" name="nama" required>
		</div>
		<div class="form-group">
			<label for="jabatan">Jabatan</label>
			<input class="form-control" type="text" value="{{ $p->pegawai_jabatan }}"name="jabatan" required>
		</div>
		<div class="form-group">
			<label for="alamat">Alamat</label> 
			<textarea class="form-control" name="alamat" required>{{ $p->pegawai_alamat }}</textarea>
		</div>
		<div class="form-group">
			<label for="nohp">No HP</label>
			<input class="form-control" type="text" value="{{ $p->pegawai_hp }}" pattern="[0-9]*" name="nohp" required>
		</div>
		<input class="btn btn-primary" type="submit" value="Ubah Data">
	</form>
	@endforeach
@endsection