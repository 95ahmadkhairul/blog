<html lang="en">
	<head>
	<meta charset="utf-8">
	<title>Blog Over Power</title>
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link href="{{ asset('/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('/font-awesome/css/font-awesome.css') }}" rel="stylesheet" />
	<link href="{{ asset('/stylish.css') }}" rel="stylesheet" />
	<link href="{{ asset('/datatables.min.css') }}" rel="stylesheet" />
	</head>
<body>
 	<div class="container">
	 	<header>
			<nav class="navbar navbar-expand-lg navbar-light bg-light border border-gray">
				<div class="collapse navbar-collapse" id="navbarText">
					<ul class="navbar-nav">
					@yield('aktif')
					</ul>
				</div>
			</nav>
		</header>
		<img src="{{ asset('/images/banner.jpg') }}" width="1110" height="170" alt=""> 
		<div class="card">
	  		<div class="card-header">
				@yield('judul_halaman')
			</div>
			<div class="card-body">
				@yield('konten')
	 		</div>
			<div class="card-footer">
				<h5 class="text-center">2019 &copy; Ahmad Khairul</h5>
			</div>
	  	</div>
	</div>
	<script type="text/javascript" src="{{ asset('/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/bootstrap/js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/ckeditor/ckeditor.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/datatables.min.js') }}"></script> 
	<script type="text/javascript" src="{{ asset('/news.js') }}"></script>     
	 @stack('scripts')
</body>
</html>