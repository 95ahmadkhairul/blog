@extends('master')

@section('aktif')
<li class="nav-item">
	<a class="nav-link" href="/blog">Home</a>
</li>
<li class="nav-item">
	<a class="nav-link" href="/pegawai">Pegawai</a>
</li>
<li class="nav-item">
	<a class="nav-link" href="/karyawan">Karyawan</a>
</li>
<li class="nav-item">
	<a class="nav-link" href="/blog/tentang">Tentang</a>
</li>
<li class="nav-item">
	<a class="nav-link active" href="/blog/kontak">Kontak</a>
</li>
@endsection

@section('judul_halaman', 'Home -> Kontak')

@section('konten')
	
<div class="row">
	<div class="col-md-2">
		<p>Adress</p>
		<p>Phone</p>
		<p>Email</p>
		<p>Facebook</p>
		<p>Instagram</p>
	</div>
	<div class="col-md-1">
		<p>:</p>
		<p>:</p>
		<p>:</p>
		<p>:</p>
		<p>:</p>
	</div>
	<div class="col-md-9">
		<p>Bumi Langgeng Cinunuk No. 35/03 Bandung District</p>
		<p>082119274080</p>
		<p>95ahmadkhairul@gmail.com</p>
		<p>facebook.com/ahmad.khairul.95</p>
		<p>instagram.com/hmadzk</p>
	</div>
</div>



@endsection