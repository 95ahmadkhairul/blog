@extends('master')

@section('aktif')
<li class="nav-item">
	<a class="nav-link" href="/blog">Home</a>
</li>
<li class="nav-item">
	<a class="nav-link" href="/pegawai">Pegawai</a>
</li>
<li class="nav-item">
	<a class="nav-link" href="/karyawan">Karyawan</a>
</li>
<li class="nav-item">
	<a class="nav-link active" href="/blog/tentang">Tentang</a>
</li>
<li class="nav-item">
	<a class="nav-link" href="/blog/kontak">Kontak</a>
</li>
@endsection

@section('judul_halaman', 'Home -> Tentang')

@section('konten')
	<p>Ini adalah halaman website biasa, yang dibuat se-biasa mungkin karena keterbatasan waktu</p>
	<p>Daftar Pustaka</p>
	<ul>
		<li><a href="https://www.malasngoding.com">Malasngoding</a></li>
		<li><a href="https://www.laravel.com/docs">Laravel Documentation</a></li>
	</ul>

@endsection