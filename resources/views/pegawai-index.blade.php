@extends('master')

@section('aktif')
<li class="nav-item">
	<a class="nav-link" href="/blog">Home</a>
</li>
<li class="nav-item">
	<a class="nav-link active" href="/pegawai">Pegawai</a>
</li>
<li class="nav-item">
	<a class="nav-link" href="/karyawan">Karyawan</a>
</li>
<li class="nav-item">
	<a class="nav-link" href="/blog/tentang">Tentang</a>
</li>
<li class="nav-item">
	<a class="nav-link" href="/blog/kontak">Kontak</a>
</li>
@endsection

@section('judul_halaman', 'Home -> Pegawai')

@section('konten')
	<p class="text-right"><a href="/pegawai/tambah">Tambah data</a></p>
	<table class="display table table-bordered" style="width:100%">
		<thead>
			<tr>
				<th>Nama</th>
				<th>Jabatan</th>
				<th>Alamat</th>
				<th>No HP</th>
				<th style="width:15%">Aksi</th>
			</tr>
		</thead>
		<tbody>
			@foreach($pegawai as $p)
			<tr>
				<td>{{ $p->pegawai_nama }}</td>
				<td>{{ $p->pegawai_jabatan }}</td>
				<td>{{ $p->pegawai_alamat }}</td>
				<td>{{ $p->pegawai_hp }}</td>
				<td>
				<a href="/pegawai/edit/{{ $p->pegawai_id }}" class="btn btn-info">
					<i class="fa fa-edit"></i> 
				</a> 
				<a href="/pegawai/delete/{{ $p->pegawai_id }}" class="btn btn-danger"> 
					<i class="fa fa-close"></i>
				</a></td>
			</tr>
			@endforeach
		</tbody>
	</table>
@endsection