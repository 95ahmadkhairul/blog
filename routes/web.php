<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/halo', function () {
	return "Halo selamat datang di tutorial Laravel";
});

//route blog
Route::get('/blog', 'BlogController@index');
Route::get('/blog/tentang', 'BlogController@tentang');
Route::get('/blog/kontak', 'BlogController@kontak');

//CRUD
Route::get('/pegawai/', 'PegawaiController@index');
Route::get('/pegawai/tambah', 'PegawaiController@tambah');
Route::post('/pegawai/simpan', 'PegawaiController@simpan');
Route::get('/pegawai/edit/{id}','PegawaiController@edit');
Route::post('/pegawai/update', 'PegawaiController@update');
Route::get('/pegawai/delete/{id}','PegawaiController@delete');

//CRUD-ELOQUENT
Route::get('/karyawan/', "KaryawanController@index");
Route::get('/karyawan/tambah', 'KaryawanController@tambah');
Route::post('/karyawan/simpan', 'KaryawanController@simpan');
Route::get('/karyawan/edit/{id}','KaryawanController@edit');
Route::post('/karyawan/update', 'KaryawanController@update');
Route::get('/karyawan/delete/{id}','KaryawanController@delete');