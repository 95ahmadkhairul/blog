<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Karyawan;
use App\Telepon;

use Session;

class KaryawanController extends Controller {
    
    public function index() {
    	
    	$karyawan = Karyawan::all();
    	return view('karyawan-index', ['karyawan' => $karyawan]);
    
    }

    public function tambah() {
    	return view('karyawan-tambah');
    }
 
 	public function simpan(Request $request) {
    	
    	$this->validate($request, $this->validationRules());
        $karyawan = new Karyawan;
        $karyawan->nama = $request->nama;
        $karyawan->alamat = $request->alamat;

        $karyawan->save();

        $telepon = new Telepon;
        $telepon->nomor_telepon = $request->nomor_telepon;

        $karyawan->telepon()->save($telepon);

        Session::flash('message', 'New data has been added successfully.');
    	return redirect('/karyawan');
    }
	
	public function edit($id) {
   		$karyawan = Karyawan::find($id);
   		return view('karyawan-edit', ['karyawan' => $karyawan]);
	}

	public function update(Request $request) {
		$karyawan = Karyawan::with('Telepon')->find($request->id);
		$this->validate($request, $this->validationRules());
	  
        $karyawan->nama = $request->nama;
	    $karyawan->alamat = $request->alamat;
	    $karyawan->telepon->nomor_telepon = $request->nomor_telepon; 

        $karyawan->push();

        Session::flash('message', 'Member has been updated successfully.');
        return redirect('/karyawan');
	}

	public function delete($id) {
        Karyawan::destroy($id);
        Session::flash('message', 'Data has been deleted successfully.');
	    return redirect('/karyawan');
	}

	private function validationRules() {
        return [
            'nama' => 'required',
            'alamat' => 'required',
            'nomor_telepon' => 'required'
        ];
    }

}
