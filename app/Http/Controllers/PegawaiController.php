<?php

namespace App\Http\Controllers;

use illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PegawaiController extends Controller
{
	
	public function index(){
    	$pegawai = DB::table('pegawai')->get();
    	return view('pegawai-index',['pegawai' => $pegawai]);
	}
 
	public function tentang(){
		return view('blog-tentang');
	}
 
	public function kontak(){
		return view('blog-kontak');
	}

	public function tambah(){
		return view('pegawai-tambah');
	}

	public function simpan(Request $request){
		DB::table('pegawai')->insert([
			'pegawai_nama' => $request->nama,
			'pegawai_jabatan' => $request->jabatan,
			'pegawai_hp' => $request->nohp,
			'pegawai_alamat' => $request->alamat
		]);
		return redirect('/pegawai');
	}

	public function edit($id){
		$pegawai = DB::table('pegawai')->where('pegawai_id',$id)->get();
		return view('pegawai-edit',['pegawai' => $pegawai]);
	}

	public function update(Request $request){
		DB::table('pegawai')->where('pegawai_id',$request->id)->update([
			'pegawai_nama' => $request->nama,
			'pegawai_jabatan' => $request->jabatan,
			'pegawai_hp' => $request->nohp,
			'pegawai_alamat' => $request->alamat
		]);
		return redirect('/pegawai');
	}

	public function delete($id){
		$pegawai = DB::table('pegawai')->where('pegawai_id',$id)->delete();
		return redirect('/pegawai');
	}
}