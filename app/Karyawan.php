<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    protected $table = "karyawan";
    protected $fillable = ['nama','alamat'];

    public function telepon() {
    	return $this->hasOne('App\Telepon');
    }

}
