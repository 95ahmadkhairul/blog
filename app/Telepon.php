<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Telepon extends Model
{
    protected $table = "telepon";
    protected $fillable = ['karyawan_id','nomor_telepon'];

    public function karyawan() {
    	return $this->belongsTo('App\Karyawan');
    }
}
